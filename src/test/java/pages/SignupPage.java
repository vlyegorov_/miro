package pages;

import com.microsoft.playwright.Page;
import io.qameta.allure.Description;
import utils.PropertiesContext;
import utils.UiHelper;

public class SignupPage {

    private final String userNameField = ".signup #name";
    private final String emailField = ".signup #email";
    private final String passwordField = ".signup #password";
    private final String termsCheckbox = "label[for='signup-terms']";
    private final String updatesCheckbox = "label[for='signup-subscribe']";
    private final String getStartedButton = "[data-autotest-id='mr-form-signup-btn-start-1']";
    private final String signUpWithGoogleButton = "#kmq-google-button";
    private final String signUpSlack = "#kmq-slack-button";
    private final String signupOffice = ".signup__btn--office365";
    private final String signUpApple = "#apple-auth";
    private final String signUpFaceBook = ".signup__btn--facebook";
    private final String aboutMiroField = ".signup__input-text";

    private final Page page;
    UiHelper uiHelper = new UiHelper();

    public SignupPage(Page page) {
        this.page = page;
    }


    public void navigate() {
        page.navigate(PropertiesContext.getInstance().getProperty("framework_baseurl"));
    }

    @Description("Sign up with details")
    public void fillUserData(String name, String email, String password) {
        page.fill(userNameField, name);
        page.fill(emailField, email);
        page.fill(passwordField, password);
    }

    public void fillHearAboutMiro(String about) {
        page.fill(aboutMiroField, about);
    }

    public void setTermsCheckbox() {
        page.check(termsCheckbox);
//        uiHelper.checkCheckBox(page, termsCheckbox);
    }

    public void setUpdatesCheckbox() {
        uiHelper.checkCheckBox(page, updatesCheckbox);
    }

    public CheckYourEmailPage clickOnSubmitButton() {
        page.click(getStartedButton);
        return new CheckYourEmailPage(page);
    }
}