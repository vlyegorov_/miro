package pages;

import com.microsoft.playwright.Page;

public class CheckYourEmailPage {

    public static String checkYourEmailHeader = ".signup__title-form";

    private final Page page;

    public CheckYourEmailPage(Page page) {
        this.page = page;
    }

    public String getHeaderText(){
        return page.innerText(checkYourEmailHeader);
    }

}