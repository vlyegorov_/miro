package utils;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Playwright;

import java.util.HashMap;
import java.util.Map;

public class WebBrowser  {

    static Playwright playwright;
    static Browser browser;

    public static BrowserType getBrowserType() {

        String browserName = PropertiesContext.getInstance().getProperty("browser");
        switch (browserName) {
            case "chrome":
                return playwright.chromium();
            case "webkit":
                return playwright.webkit();
            case "firefox":
                return playwright.firefox();
            default:
                throw new IllegalArgumentException();
        }
    }


    public static Browser getBrowser() {
        playwright = Playwright.create();
        switch (OperationSystemInfo.getOs()) {
            case UNIX:
                browser = getBrowserType().launch();
                break;
            case WINDOWS:
            default:
                browser = getBrowserType()
                        .launch(new BrowserType.LaunchOptions()
                                .setHeadless(false).setSlowMo(500));
                break;
        }
        return browser;
    }

}