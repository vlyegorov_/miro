package tests;

import entities.users.UserFactory;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import pages.CheckYourEmailPage;
import pages.SignupPage;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.parallel.ExecutionMode.CONCURRENT;


public class SignUpTest extends BaseTestCase {

    @Test
    public void signupWithEmail() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }

    @Test
    public void signupWithEmail2() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }

    @Test
    public void signupWithEmail3() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }

    @Test
    public void signupWithEmail4() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }

    @Test
    public void signupWithEmail5() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }
}
