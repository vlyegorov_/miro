package tests;

import entities.users.UserFactory;
import org.junit.jupiter.api.Test;
import pages.CheckYourEmailPage;

import static org.assertj.core.api.Assertions.assertThat;


public class SignUpTest4 extends BaseTestCase {

    @Test
    public void signupWithEmail() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }

    @Test
    public void signupWithEmail2() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }

    @Test
    public void signupWithEmail3() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }

    @Test
    public void signupWithEmail4() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }

    @Test
    public void signupWithEmail5() {
        signupPage.fillUserData(UserFactory.createUser().getName(), UserFactory.createUser().getEmail(),
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        String validationEmail = checkYourEmailPage.getHeaderText();
        assertThat(validationEmail).contains("Check your email");
    }
}
