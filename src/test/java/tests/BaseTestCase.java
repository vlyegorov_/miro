package tests;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import lombok.Data;
import org.junit.jupiter.api.*;
import pages.SignupPage;
import utils.PropertiesContext;

@Data
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BaseTestCase {
    // Shared between all tests in the class.
    Playwright playwright;
    Browser browser;

    @BeforeAll
    void launchBrowser() {
        playwright = Playwright.create();
        browser = playwright.chromium().launch();
    }

    @AfterAll
    void closeBrowser() {
        playwright.close();
    }

    // New instance for each test method.
    BrowserContext context;
    Page page;
    SignupPage signupPage;

    @BeforeEach
    void createContextAndPage() {
        context = browser.newContext();
        page = context.newPage();
        openBasePage();
    }

    @AfterEach
    void closeContext() {
        context.close();
    }

    public void openBasePage() {
        page.navigate(PropertiesContext.getInstance().getProperty("framework_baseurl"));
        signupPage = new SignupPage(getPage());
    }
}